% xsens orientations
%% first xsens
disp("first xsens")
[file_xsens, path_xsens] = uigetfile(".csv");
importfile (strcat(path_xsens,file_xsens), [1, Inf]);
quat_xsens_1 = ans(:, 3:6);

%% second xsens
disp("second xsens")
[file_xsens, path_xsens] = uigetfile(".csv");
importfile (strcat(path_xsens,file_xsens), [1, Inf]);
quat_xsens_2 = ans(:, 3:6);
%% xsens_1
delta_up1_xsens_1 = 0;
delta_down1_xsens_1 = 0;
delta_up2_xsens_1 = 0;
delta_down2_xsens_1 = 0;
delta_up3_xsens_1 = 0;
delta_down3_xsens_1 = 0;
th_xsens_1 = 250;

x = quat_xsens_1(:,1);
y = quat_xsens_1(:,2);
z = quat_xsens_1(:,3);
w = quat_xsens_1(:,4);
new_quat_xsens_1 = [w, x, y, z];
[eulRot_xsens_1(:,1),eulRot_xsens_1(:,2),eulRot_xsens_1(:,3)] = quat2angle(new_quat_xsens_1, "ZYX");
eulRot_xsens_1(:,1) = rad2deg(eulRot_xsens_1(:,1));
eulRot_xsens_1(:,2) = rad2deg(eulRot_xsens_1(:,2));
eulRot_xsens_1(:,3) = rad2deg(eulRot_xsens_1(:,3));

eulRot_xsens_1_orig = eulRot_xsens_1;
delta_y_xsens_1 = zeros (size(eulRot_xsens_1));
%adjusting eulRot_xsens_1
delta_length_xsens_1 = length(delta_y_xsens_1)-1;
delta_y_xsens_1(1:delta_length_xsens_1, 1) = diff(eulRot_xsens_1(:,1));
delta_y_xsens_1(1:delta_length_xsens_1, 2) = diff(eulRot_xsens_1(:,2));
delta_y_xsens_1(1:delta_length_xsens_1, 3) = diff(eulRot_xsens_1(:,3));
for i = 1:length(eulRot_xsens_1)
    
    eulRot_xsens_1(i,1) = eulRot_xsens_1(i,1) - 360 * delta_up1_xsens_1 + 360 * delta_down1_xsens_1;
    if delta_y_xsens_1(i,1) > th_xsens_1
        delta_up1_xsens_1 = delta_up1_xsens_1 + 1;
    elseif delta_y_xsens_1(i,1) < -th_xsens_1  
        delta_down1_xsens_1 = delta_down1_xsens_1 + 1;
    end
    

    eulRot_xsens_1(i,2) = eulRot_xsens_1(i,2) - 360 * delta_up2_xsens_1 + 360 * delta_down2_xsens_1;
    if delta_y_xsens_1(i,2) > th_xsens_1
        delta_up2_xsens_1 = delta_up2_xsens_1 + 1;
    elseif delta_y_xsens_1(i,2) < -th_xsens_1  
        delta_down2_xsens_1 = delta_down2_xsens_1 + 1;
    end
    
    eulRot_xsens_1(i,3) = eulRot_xsens_1(i,3) - 360 * delta_up3_xsens_1 + 360 * delta_down3_xsens_1;
    if delta_y_xsens_1(i,3) > th_xsens_1
        delta_up3_xsens_1 = delta_up3_xsens_1 + 1;
    elseif delta_y_xsens_1(i,3) < -th_xsens_1  
        delta_down3_xsens_1 = delta_down3_xsens_1 + 1;
    end
end

% plot y
figure;
subplot(2,1,1);
plot (eulRot_xsens_1(:,2));
% hold on
% plot (delta_y_xsens_1(:,2));
title("Eulero y corretto xsens");
subplot(2,1,2);
plot (eulRot_xsens_1_orig(:,2));
hold on
plot (delta_y_xsens_1(:,2));
title("Eulero y originale xsens");

% plot 1
figure;
subplot(2,1,1);
plot (eulRot_xsens_1(:,1));
% hold on
% plot (delta_y_xsens_1(:,1));
title("eulero x corretto xsens");
subplot(2,1,2);
plot (eulRot_xsens_1_orig(:,1));
hold on
plot (delta_y_xsens_1(:,1));
title("eulero x originale xsens");

% plot 3
figure;
subplot(2,1,1);
plot (eulRot_xsens_1(:,3));
% hold on
% plot (delta_y_xsens_1(:,3));
title("eulero z corretto xsens");
subplot(2,1,2);
plot (eulRot_xsens_1_orig(:,3));
hold on
plot (delta_y_xsens_1(:,3));
title("eulero z originale xsens");


%% xsens_2
delta_up1_xsens_2 = 0;
delta_down1_xsens_2 = 0;
delta_up2_xsens_2 = 0;
delta_down2_xsens_2 = 0;
delta_up3_xsens_2 = 0;
delta_down3_xsens_2 = 0;
th_xsens_2 = 250;

w = quat_xsens_2(:,1);
x = quat_xsens_2(:,2);
y = quat_xsens_2(:,3);
z = quat_xsens_2(:,4);
new_quat_xsens_2 = [w, x, y, z];
[eulRot_xsens_2(:,3),eulRot_xsens_2(:,1),eulRot_xsens_2(:,2)] = quat2angle(new_quat_xsens_2, "ZXY");
eulRot_xsens_2(:,1) = rad2deg(eulRot_xsens_2(:,1));
eulRot_xsens_2(:,2) = rad2deg(eulRot_xsens_2(:,2));
eulRot_xsens_2(:,3) = rad2deg(eulRot_xsens_2(:,3));

eulRot_xsens_2_orig = eulRot_xsens_2;
delta_y_xsens_2 = zeros (size(eulRot_xsens_2));
%adjusting eulRot_xsens_2
delta_length_xsens_2 = length(delta_y_xsens_2)-1;
delta_y_xsens_2(1:delta_length_xsens_2, 1) = diff(eulRot_xsens_2(:,1));
delta_y_xsens_2(1:delta_length_xsens_2, 2) = diff(eulRot_xsens_2(:,2));
delta_y_xsens_2(1:delta_length_xsens_2, 3) = diff(eulRot_xsens_2(:,3));
for i = 1:length(eulRot_xsens_2)
    
    eulRot_xsens_2(i,1) = eulRot_xsens_2(i,1) - 360 * delta_up1_xsens_2 + 360 * delta_down1_xsens_2;
    if delta_y_xsens_2(i,1) > th_xsens_2
        delta_up1_xsens_2 = delta_up1_xsens_2 + 1;
    elseif delta_y_xsens_2(i,1) < -th_xsens_2  
        delta_down1_xsens_2 = delta_down1_xsens_2 + 1;
    end
    

    eulRot_xsens_2(i,2) = eulRot_xsens_2(i,2) - 360 * delta_up2_xsens_2 + 360 * delta_down2_xsens_2;
    if delta_y_xsens_2(i,2) > th_xsens_2
        delta_up2_xsens_2 = delta_up2_xsens_2 + 1;
    elseif delta_y_xsens_2(i,2) < -th_xsens_2  
        delta_down2_xsens_2 = delta_down2_xsens_2 + 1;
    end
    
    eulRot_xsens_2(i,3) = eulRot_xsens_2(i,3) - 360 * delta_up3_xsens_2 + 360 * delta_down3_xsens_2;
    if delta_y_xsens_2(i,3) > th_xsens_2
        delta_up3_xsens_2 = delta_up3_xsens_2 + 1;
    elseif delta_y_xsens_2(i,3) < -th_xsens_2  
        delta_down3_xsens_2 = delta_down3_xsens_2 + 1;
    end
end

% plot y
figure;
subplot(2,1,1);
plot (eulRot_xsens_2(:,2));
hold on
plot (delta_y_xsens_2(:,2));
title("Eulero y corretto");
subplot(2,1,2);
plot (eulRot_xsens_2_orig(:,2));
hold on
plot (delta_y_xsens_2(:,2));
title("Eulero y originale");

% plot 1
figure;
subplot(2,1,1);
plot (eulRot_xsens_2(:,1));
hold on
plot (delta_y_xsens_2(:,1));
title("eulero x corretto");
subplot(2,1,2);
plot (eulRot_xsens_2_orig(:,1));
hold on
plot (delta_y_xsens_2(:,1));
title("eulero x originale");

% plot 3
figure;
subplot(2,1,1);
plot (eulRot_xsens_2(:,3));
hold on
%plot (delta_y_xsens_2(:,3));
title("eulero z corretto");
subplot(2,1,2);
plot (eulRot_xsens_2_orig(:,3));
hold on
plot (delta_y_xsens_2(:,3));
title("eulero z originale");

%% diff
eulRot_xsens_diff = euRot_xsens_2 - eulRot_xsens1;