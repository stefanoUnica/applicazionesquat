%% This script is useful in order to calculate the position of the phone 
%% using the accerelation measurement
clear all;

%% Extraction of acceleration data
% acc = Sensors19042020(: , 4:6);
[file_acc,path_acc] = uigetfile(".txt");
importfile_ACC (strcat(path_acc,file_acc), [1, Inf]);
%importfile_ACC_old (strcat(path_acc,file_acc), [1, Inf]);

acc = ans;
resample_Android_sensor_signal;
acc = acc_rsmpl;
%% calculation of a_avg acceleration
for i = 1:length(acc)    
    if i > 1
        % x acc_avg
        acc_avg(i,1) = (acc (i,2) + acc (i - 1,2))/2;
        % y acc_avg
        acc_avg(i,2) = (acc (i,3) + acc (i - 1,3))/2;
        % z acc_avg
        acc_avg(i,3) = (acc (i,4) + acc (i - 1,4))/2;
    end
end

%% velocity calculation
% time supposing a frequency of 256Hz
Fs = 1000;
T = 1/Fs;
t = 1 : length(acc);
t = (1/Fs).*t;
vel = zeros (size(acc));
% velocity calculation
for i = 1:length(acc)    
    if i > 1
        % x acc_avg
        vel(i,1) = (t(i)-t(i-1))* acc_avg(i-1,1) + vel(i-1,1);
        % y acc_avg
        vel(i,2) = (t(i)-t(i-1))* acc_avg(i-1,2) + vel(i-1,2);
        % z acc_avg
        vel(i,3) = (t(i)-t(i-1))* acc_avg(i-1,3) + vel(i-1,3);
    end
end
%% average velocity
vel_bsln = zeros(3,1);
for i = 1:length(acc)    
    if i > 1

        % x acc_avg
        vel_avg(i,1) = (vel (i,1) + vel (i - 1,1))/2;
        % y acc_avg
        vel_avg(i,2) = (vel (i,2) + vel (i - 1,2))/2;
        % z acc_avg
        vel_avg(i,3) = (vel (i,3) + vel (i - 1,3))/2;
        % x acc_avg
        vel_bsln(1) = vel_avg(i,1) + vel_bsln(1);
        % y acc_avg
        vel_bsln(2) = vel_avg(i,2) + vel_bsln(2);
        % z acc_avg
        vel_bsln(3) = vel_avg(i,3) + vel_bsln(3);
    end
end
% subtracting the average
vel_bsln(1)= vel_bsln(1)/ length(acc);
vel_avg(:,1) = vel_avg(:,1) - vel_bsln(1);

% filtering x
filt_vel_avg(:,1) = highpass(vel_avg(2:length(vel_avg),1),0.2,Fs);
% filtering y
filt_vel_avg(:,2) = highpass(vel_avg(2:length(vel_avg),2),0.2,Fs);
% filtering z
filt_vel_avg(:,3) = highpass(vel_avg(2:length(vel_avg),3),0.2,Fs);

%close all;

%% position calculation
pos = zeros (size(acc));
for i = 1:length(acc)    
    if i > 1
        % x acc_avg
        pos(i, 1) = pos(i-1, 1) + (t(i)-t(i-1)) * filt_vel_avg(i-1,1) + (1/2)*acc_avg(i, 1) * (t(i)-t(i-1))^2;
        % y acc_avg
        pos(i, 2) = pos(i-1, 2) + (t(i)-t(i-1)) * filt_vel_avg(i-1,2) + (1/2) * acc_avg(i, 2) * (t(i)-t(i-1))^2;
        % z acc_avg
        pos(i, 3) = pos(i-1, 3) + (t(i)-t(i-1)) * filt_vel_avg(i-1,3) + (1/2)*acc_avg(i, 3) * (t(i)-t(i-1))^2;    
    end
end

%% Plot Acceleration xyz
figure;
t = 1/Fs*(1:length(acc)); 
subplot(3,1,1);
plot(t(1,:), acc(:,2));
xlabel("sec");
ylabel ("m/s^2");
title ("x acceleration");
subplot(3,1,2);
plot(t(1,:), acc(:,3));
xlabel("sec");
ylabel ("m/s^2");
title ("y acceleration");
subplot(3,1,3);
plot(t(1,:), acc(:,4));
xlabel("sec");
ylabel ("m/s^2");
title ("z acceleration");

%% velocity of repetition
