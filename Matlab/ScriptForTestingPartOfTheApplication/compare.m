% comparing xsens and android
% subplot x
figure;
subplot(2,1,1);
plot (eulRot(:,1));
hold on
title("eulero x android");
subplot(2,1,2);
plot (eulRot_xsens_1(:,1));
title("eulero x xsens");

% subplot y
figure;
subplot(2,1,1);
plot (eulRot(:,2));
hold on
title("eulero y android");
subplot(2,1,2);
plot (eulRot_xsens_1(:,2));
title("eulero y xsens");

% subplot z
figure;
subplot(2,1,1);
plot (eulRot(:,3));
hold on
title("eulero z android");
subplot(2,1,2);
plot (eulRot_xsens_1(:,3));
title("eulero z xsens");