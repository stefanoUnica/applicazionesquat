% Plot velocity
figure;
subplot(3,1,1);
plot(vel(:,1));
title ("x velocity");
subplot(3,1,2);
plot(vel(:,2));
title ("y velocity");
subplot(3,1,3);
plot(vel(:,3));
title ("z velocity");

%% Plot filtered velocity
figure;
subplot(3,1,1);
plot(filt_vel_avg(:,1));
title ("x filtered velocity");
subplot(3,1,2);
plot(filt_vel_avg(:,2));
title ("y filtered velocity");
subplot(3,1,3);
plot(filt_vel_avg(:,3));
title ("z filtered velocity");

%% Plot Acceleration xyz
figure;
t = 1/Fs*(1:length(acc)); 
subplot(3,1,1);
plot(t(1,:), acc(:,1));
xlabel("sec");
ylabel ("m/s^2");
title ("x acceleration");
subplot(3,1,2);
plot(t(1,:), acc(:,2));
xlabel("sec");
ylabel ("m/s^2");
title ("y acceleration");
subplot(3,1,3);
plot(t(1,:), acc(:,3));
xlabel("sec");
ylabel ("m/s^2");
title ("z acceleration");

%% Plot Acceleration MOTION SENSORS
figure;
subplot(3,1,1);
plot(FILESENSORACC202051205256(:,1));
title ("x acceleration");
subplot(3,1,2);
plot(FILESENSORACC202051205256(:,2));
title ("y acceleration");
subplot(3,1,3);
plot(FILESENSORACC202051205256(:,3));
title ("z acceleration");

%% Plot positions
% figure;
% subplot(3,1,1);
% plot(Sensors19042020(:,7));
% title ("x positions");
% subplot(3,1,2);
% plot(Sensors19042020(:,8));
% title ("y positions");
% subplot(3,1,3);
% plot(Sensors19042020(:,9));
% title ("z positions");

%% Plot positions
figure;
subplot(3,1,1);
plot(pos(:,1));
title ("x positions");
subplot(3,1,2);
plot(pos(:,2));
title ("y positions");
subplot(3,1,3);
plot(pos(:,3));
title ("z positions");

%% animation
plot3(pos(:,1), pos(:,2), pos(:,3));