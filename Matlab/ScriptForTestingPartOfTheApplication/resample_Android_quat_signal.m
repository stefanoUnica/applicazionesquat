%% Resampling an android sensor signal with Matlab
% clear all workspace
%clear all; %#ok<CLALL>
% closing all windows
%close all;
Fs = 200;
%% Import the signal
%[file_acc,path_acc] = uigetfile(".txt");
%acc = importfile_ACC (strcat(path_acc,file_acc), [1, Inf]);

%% Calculate t array
t = zeros(length(quat),1);
tmill = t;
for i = 2:length(quat)
    t(i) = (quat (i, 1)- quat(i-1,1)) + t(i-1);
    tmill(i) = t(i)/1000;
end

%% plotting graphs
figure;
[quat_rsmpl(:,2), tmill_rsmpl(:,1)] = resample(quat(:,2),tmill,Fs);
plot(tmill,quat(:,2),'.-', tmill_rsmpl(:,1),quat_rsmpl(:,2),'o-')
legend('Original','Resampled')
figure;
[quat_rsmpl(:,3), tmill_rsmpl(:,2)] = resample(quat(:,3),tmill,Fs);
plot(tmill,quat(:,3),'.-', tmill_rsmpl(:,2),quat_rsmpl(:,3),'o-')
legend('Original','Resampled')
figure;
[quat_rsmpl(:,4), tmill_rsmpl(:,3)] = resample(quat(:,4),tmill,Fs);
plot(tmill,acc(:,4),'.-', tmill_rsmpl(:,3),quat_rsmpl(:,4),'o-')
legend('Original','Resampled')
