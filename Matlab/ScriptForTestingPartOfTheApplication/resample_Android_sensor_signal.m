%% Resampling an android sensor signal with Matlab
% clear all workspace
%clear all; %#ok<CLALL>
% closing all windows
%close all;
Fs = 100;
%% Import the signal
%[file_acc,path_acc] = uigetfile(".txt");
%acc = importfile_ACC (strcat(path_acc,file_acc), [1, Inf]);

%% Calculate t array
t = zeros(length(acc),1);
tmill = t;
for i = 2:length(acc)
    t(i) = (acc (i, 1)- acc(i-1,1)) + t(i-1);
    tmill(i) = t(i)/1000;
end

%% plotting graphs
figure;
[acc_rsmpl(:,2), tmill_rsmpl(:,1)] = resample(acc(:,2),tmill,Fs);
plot(tmill,acc(:,2),'.-', tmill_rsmpl(:,1),acc_rsmpl(:,2),'o-')
legend('Original','Resampled')
figure;
[acc_rsmpl(:,3), tmill_rsmpl(:,2)] = resample(acc(:,3),tmill,Fs);
plot(tmill,acc(:,3),'.-', tmill_rsmpl(:,2),acc_rsmpl(:,3),'o-')
legend('Original','Resampled')
figure;
[acc_rsmpl(:,4), tmill_rsmpl(:,3)] = resample(acc(:,4),tmill,Fs);
plot(tmill,acc(:,4),'.-', tmill_rsmpl(:,3),acc_rsmpl(:,4),'o-')
legend('Original','Resampled')