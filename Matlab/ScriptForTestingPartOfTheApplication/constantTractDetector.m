%% offline analysis
% This script is useful in order to find tract of stable signal in offline 
% analysis
% define theta

Fs = 100; %Hz
w = 50; % window size
tau = 1; %first index
p = 1; %window pace
c_1 = 0; % counter first component
c_2 = 0; % counter second component
c_3 = 0; % counter third component
c_4 = 0; % counter fourth component
fiveSecs_1 = 0;
fiveSecs_2 = 0;
fiveSecs_3 = 0;
fiveSecs_4 = 0;
interval_accepted =0.001;

while ((tau + p)+ w < length(theta))
    
    % sum elements tau
    sum_1 = 0;
    sum_2 = 0;
    sum_3 = 0;
    sum_4 = 0;
    
    for i = 1 : (w - 1)
        index = (tau + w)-i;
        sum_1 = theta(index, 2) + sum_1;
        sum_2 = theta(index, 3) + sum_2;
        sum_3 = theta(index, 4) + sum_3;
        sum_4 = theta(index, 5) + sum_4;

    end
    sum_1 = sum_1/w;
    sum_2 = sum_2/w;
    sum_3 = sum_3/w;
    sum_4 = sum_4/w;
    % sum elements tau + p
    sum_p_1 = 0;
    sum_p_2 = 0;
    sum_p_3 = 0;
    sum_p_4 = 0;

    for i = 1 : (w - 1)
        index_p = ((tau + p)+ w)-i;
        sum_p_1 = theta(index_p, 2) + sum_p_1;
        sum_p_2 = theta(index_p, 3) + sum_p_2;
        sum_p_3 = theta(index_p, 4) + sum_p_3;
        sum_p_4 = theta(index_p, 5) + sum_p_4;

    end
    sum_p_1 = sum_p_1/w;
    sum_p_2 = sum_p_2/w;
    sum_p_3 = sum_p_3/w;
    sum_p_4 = sum_p_4/w;
    % comparing first component
    if (sum_p_1 > (1 - interval_accepted)*sum_1 && sum_p_1 <(1 + interval_accepted)*sum_1)
        c_1 = c_1 + 1;
        if((tau + c_1*p + w) - tau > 5*Fs)
            disp ('completed');
            fiveSecs_1 = fiveSecs_1 + 1;
            c_1 = 0;
        end
        disp('Stable region detected');
        movement_1(tau) = 1;
    else
        disp('Movement region detected');
        movement_1(tau) = 0;
        c_1 = 0;
    end
        % comparing second component
    if (sum_p_2 > (1 - interval_accepted)*sum_2 && sum_p_2 < (1 + interval_accepted)*sum_2)
        c_2 = c_2 + 1;
        if((tau + c_2*p + w) - tau > 5*Fs)
            disp ('completed');
            fiveSecs_2 = fiveSecs_2 + 1;
            c_2 = 0;
        end
        disp('Stable region detected');
        movement_2(tau) = 1;
    else
        disp('Movement region detected');
        movement_2(tau) = 0;
        c_2 = 0;
    end
            % comparing third component
    if (abs(sum_p_3) > abs((1 - interval_accepted)*sum_3) && abs(sum_p_3) < abs((1 + interval_accepted)*sum_3))
        c_3 = c_3 + 1;
        if((tau + c_3*p + w) - tau > 5*Fs)
            disp ('completed');
            fiveSecs_3 = fiveSecs_3 + 1;
            c_3 = 0;
        end
        disp('Stable region detected');
        movement_3(tau) = 1;
    else
        disp('Movement region detected');
        movement_3(tau) = 0;
        c_3 = 0;
    end
    % comparing fourth component
    if (abs(sum_p_4) > abs((1 - interval_accepted)*sum_4) && abs(sum_p_4) < abs((1 + interval_accepted)*sum_4))
        c_4 = c_4 + 1;
        if((tau + c_4*p + w) - tau > 5*Fs)
            disp ('completed');
            fiveSecs_4 = fiveSecs_4 + 1;
            c_4 = 0;
        end
        disp('Stable region detected');
        movement_4(tau) = 1;
    else
        disp('Movement region detected');
        movement_4(tau) = 0;
        c_4 = 0;
    end
    
    % increment tau
    tau = tau + p;
end
close all
figure;
plot(movement_1)
hold on
plot(theta(:,2))
title('first component evaluation');

figure;
plot(movement_2)
hold on
plot(theta(:,3))
title('second component evaluation');

figure;
plot(movement_3)
hold on
plot(theta(:,4))
title('third component evaluation');

figure;
plot(movement_4)
hold on
plot(theta(:,5))
title('fourth component evaluation');
