%% This script is useful in order to calculate the rotation of the phone 


%% Extraction of quaternions data
[file_quat, path_quat] = uigetfile(".txt");
importfile_QUAT (strcat(path_quat,file_quat), [1, Inf]);

%% Dichiarazione di variabili funzionali
delta_up = 0;
delta_down = 0;
delta_up2 = 0;
delta_down2 = 0;
delta_up3 = 0;
delta_down3 = 0;
th = 200;

% assgnazioni varie
quat = ans;
x = quat(100:end,2); %componente w di quaterion
y = quat(100:end,3); %componente x di quaterion
z = quat(100:end,4);
w = quat(100:end,5);
new_quat = [w, x, y, z];
[eulRot(:,1),eulRot(:,2),eulRot(:,3)] = quat2angle(new_quat, "XYZ");
eulRot(:,1) = rad2deg(eulRot(:,1));
eulRot(:,2) = rad2deg(eulRot(:,2));
eulRot(:,3) = rad2deg(eulRot(:,3));

eulRot_orig = eulRot;
delta_y = zeros (size(eulRot));
%adjusting eulRot
delta_length = length(delta_y)-1;
delta_y(1:delta_length, 1) = diff(eulRot(:,1));
delta_y(1:delta_length, 2) = diff(eulRot(:,2));
delta_y(1:delta_length, 3) = diff(eulRot(:,3));

for i = 1:length(eulRot)
    
    eulRot(i,1) = eulRot(i,1) - 360 * delta_up + 360 * delta_down;
    if delta_y(i,1) > th
        delta_up = delta_up + 1;
    elseif delta_y(i,1) < -th  
        delta_down = delta_down + 1;
    end
    

    eulRot(i,2) = eulRot(i,2) - 360 * delta_up2 + 360 * delta_down2;
    if delta_y(i,2) > th
        delta_up2 = delta_up2 + 1;
    elseif delta_y(i,2) < -th  
        delta_down2 = delta_down2 + 1;
    end
    
    eulRot(i,3) = eulRot(i,3) - 360 * delta_up3 + 360 * delta_down3;
    if delta_y(i,3) > th
        delta_up3 = delta_up3 + 1;
    elseif delta_y(i,3) < -th  
        delta_down3 = delta_down3 + 1;
    end
end

% plot y
figure;
subplot(2,1,1);
plot (eulRot(:,2));
% hold on
% plot (delta_y(:,2));
title("eulero y corretto");
subplot(2,1,2);
plot (eulRot_orig(:,2));
hold on
plot (delta_y(:,2));
title("eulero y originale");

% plot x
figure;
subplot(2,1,1);
plot (eulRot(:,1));
% hold on
% plot (delta_y(:,1));
title("eulero x corretto");
subplot(2,1,2);
plot (eulRot_orig(:,1));
hold on
plot (delta_y(:,1));
title("eulero x originale");

% plot z
figure;
subplot(2,1,1);
plot (eulRot(:,3));
%hold on
%plot (delta_y(:,3));
title("eulero z corretto");
subplot(2,1,2);
plot (eulRot_orig(:,3));
hold on
plot (delta_y(:,3));
title("eulero z originale");


