package ojb.MotionSensors.MyHelper;

import ojb.MotionSensors.MainActivityFragment;
import ojb.MotionSensors.MainActivity;

/**
 * Created by Administrator on 15/06/2018.
 */

public class MyGlobals {
    public static MainActivity activity;
    public static MainActivityFragment fragment;

    public static Object getSystemService(String name)
    {
        return (activity != null) ? activity.getSystemService(name) : null;
    }

    public static void displaySensorsInfo(int SensorsCount)
    {
        if (fragment != null)
        {
            fragment.MyDisplaySensorsInfo(SensorsCount);
        }
    }

    public static void processEvent(int sensorID, Object [] info)
    {
        if (fragment != null)
        {
            fragment.MyProcessSensorEvent(sensorID, info);
        }
    }

    public static boolean isRunning()
    {
        return (fragment != null) ? fragment.MyIsRunning() : false;
    }

    public static void toggleRunning()
    {
        if (fragment != null)
        {
            fragment.MyToggleRunning();
        }
    }

    public static void makeToast(int msg)
    {
        if (activity != null)
        {
            activity.makeToast(msg);
        }
    }

    public static void makeToastRaw(String msg)
    {
        if (activity != null)
        {
            activity.makeToastRaw(msg);
        }
    }
}
