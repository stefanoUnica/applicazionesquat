package ojb.MotionSensors.MyHelper;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.hardware.SensorEventListener;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ojb.MotionSensors.MainActivity;
import ojb.MotionSensors.MainActivityFragment;

import static android.content.Context.SENSOR_SERVICE;
import static java.lang.Float.parseFloat;

/**
 * Created by Administrator on 15/06/2018.
 */

public class MyMgrSensors implements SensorEventListener {

    public static float deg[] = new float[3];
    private final SensorManager mSensorManager;
    public static float[] mGravity;
    public static float[] mGeomagnetic;
    private int SensorsAllowedCount;
    private boolean IsPolling;
    public static float[] prevR;
    public static float[] R;
    public static float[] I;

    public float[] angleChange;
    private List<Sensor> SensorsList;
    boolean first_time = true;
    boolean relative_rotation_success = false;

    // contros the first init of variables
    boolean once = true;
    public MyMgrSensors()
    {
        mSensorManager = (SensorManager) MyGlobals.getSystemService(SENSOR_SERVICE);
    }

    public boolean IsRunning()
    {
        return IsPolling;
    }

    public void ToggleRunning()
    {
        if (IsPolling == true)
        {
            resetSensors(true);
        }
        else
        {
            registerSensors();
        }
        IsPolling = !IsPolling;
    }

    protected void resetSensors(boolean destroy)
    {
        SensorsAllowedCount = 0;

        if (mSensorManager == null)
        {
            SensorsList = null;
        }
        else
        {
            mSensorManager.unregisterListener(this);
            SensorsList = (destroy) ? null : mSensorManager.getSensorList(Sensor.TYPE_ALL);
        }
    }

    protected void registerSensors()
    {
        resetSensors(false);

        if (SensorsList != null)
        {
            for (Sensor sensor : SensorsList) {
                switch (sensor.getType())
                {
                    // https://developer.android.com/guide/topics/sensors/sensors_motion
                    case Sensor.TYPE_MAGNETIC_FIELD:
                    case Sensor.TYPE_ACCELEROMETER:
                    case Sensor.TYPE_ACCELEROMETER_UNCALIBRATED:
                    case Sensor.TYPE_GRAVITY:
                    case Sensor.TYPE_GYROSCOPE:
                    case Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR:
                    case Sensor.TYPE_GYROSCOPE_UNCALIBRATED:
                    case Sensor.TYPE_LINEAR_ACCELERATION:
                    case Sensor.TYPE_ROTATION_VECTOR:
                    case Sensor.TYPE_SIGNIFICANT_MOTION:
                    case Sensor.TYPE_STEP_COUNTER:
                    case Sensor.TYPE_STEP_DETECTOR:
                        try{
                            mSensorManager.registerListener(this, sensor, sensor.getType(), SensorManager.SENSOR_DELAY_GAME);
                            SensorsAllowedCount++;
                        }catch(Exception e){}
                        break;

                    default:
                        break;
                }
            }
        }

        MyGlobals.displaySensorsInfo(SensorsAllowedCount);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
        // Nothing needed here
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent)
    {
        if (once) {
            R = new float[9];
            I = new float[9];
            prevR = new float[9];
            angleChange = new float[3];
            once = false;
        }
        MyGlobals.processEvent(sensorEvent.sensor.hashCode(), eventToStrings(sensorEvent));
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = sensorEvent.values;
        if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = sensorEvent.values;
        Log.d("mGeomagnetic", "Value: " + Arrays.toString(mGeomagnetic));
        Log.d("mGravity", "Value: " + Arrays.toString(mGravity));
        if (mGravity != null && mGeomagnetic != null) {

            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);

                deg[0] = (float) (orientation[0] * (180/3.14));
                deg[1] = (float) (orientation[1] * (180/3.14));
                deg[2] = (float) (orientation[2] * (180/3.14));
                Log.w("Debug delle rotazioni", "Ecco le rotazioni:" + Arrays.toString(deg) ); // orientation contains: azimut, pitch and roll
                String time_Euler = String.valueOf(System.currentTimeMillis());
                MainActivity.writer_Euler.print(
                        time_Euler + "," +
                                deg[0] + ","
                                + deg[1] + ","
                                + deg[2] +
                                "\r\n");
                String rotZ = String.valueOf(deg[0]);
                String rotX = String.valueOf(deg[1]);
                String rotY = String.valueOf(deg[2]);
                MainActivityFragment.rotations = setRotationValue (rotX, rotY, rotZ);
                if (first_time){
                    //getting rotation matrix at button click
                    relative_rotation_success = SensorManager.getRotationMatrix(prevR, I, mGravity, mGeomagnetic);
                    first_time = false;
                }
                if (!first_time && MainActivity.relative_rotation_on && relative_rotation_success) {
                    //getting relative rotation
                    Log.d("prevR", prevR.toString());
                    Log.d("R", R.toString());

                    SensorManager.getAngleChange(angleChange, R, prevR);
                    deg[0] = (float) (angleChange[0] * (180 / 3.14));
                    deg[1] = (float) (angleChange[1] * (180 / 3.14));
                    deg[2] = (float) (angleChange[2] * (180 / 3.14));
                    MainActivity.writer_RelativeRotation.print(
                            time_Euler + "," +
                                    angleChange[0] + ","
                                    + angleChange[1] + ","
                                    + angleChange[2] +
                                    "\r\n");
                    String rel_rotZ = String.valueOf(deg[0]);
                    String rel_rotX = String.valueOf(deg[1]);
                    String rel_rotY = String.valueOf(deg[2]);
                    MainActivityFragment.relative_rotations = setRotationValue(rel_rotX, rel_rotY, rel_rotZ);
                }
            }


        }
    }

    protected Object[] eventToStrings(SensorEvent sensorEvent)
    {
        List<Object> infoList = new ArrayList<Object>();
        if (sensorEvent != null)
        {
            infoList.add(sensorEvent.sensor.getName());
            infoList.add(sensorEvent.sensor.getStringType());
            infoList.add(sensorEvent.sensor.getType());
            for (float f: sensorEvent.values )
            {
                infoList.add(f);
            }
        }
        return  (Object []) infoList.toArray();
    }

    public String ToRawString(Object[] info)
    {
        String s = "";
        if ((info != null) && (info.length > 0))
        {
            int count = info.length;
            s = (String) info[0];
            for (int i=1; i < count; i++) {
                s += ("\t" + info[i]);
            }
        }
        return s;
    }


    public String ToString(Object[] info)
    {
        String s = "";
        if ((info != null) && (info.length > 0))
        {
            try
            {
                s = "" + info[0] + "[" + info[1] + "]:"; /* sensor name + sensor string type */

                int sensorType = (int) info[2];
                switch (sensorType)
                {
                    case Sensor.TYPE_ACCELEROMETER:
                        /* m/s^2 */
                        s = s + " X[" + info[3] + "]";
                        s = s + " Y[" + info[4] + "]";
                        s = s + " Z[" + info[5] + "]";
                        //*********** added ****************
                        String time_Accelerometer = String.valueOf(System.currentTimeMillis());

                        MainActivity.writer_Accelerometer.print(
                                time_Accelerometer + "," +
                                info[3] + ","
                                        + info[4]+ ","
                                        + info[5]+
                                "\r\n");
                        //*********** added ****************

                        break;
                    case Sensor.TYPE_ACCELEROMETER_UNCALIBRATED:
                        /* m/s^2 */
                        s = s + " X[" + info[3] + "]";
                        s = s + " Y[" + info[4] + "]";
                        s = s + " Z[" + info[5] + "]";

                        s = s + " Xbias[" + info[6] + "]";
                        s = s + " Ybias[" + info[7] + "]";
                        s = s + " Zbias[" + info[8] + "]";
                        //*********** added ****************
                        String time_Accelerometer_Un = String.valueOf(System.currentTimeMillis());

                        MainActivity.writer_Acc_Un.print(
                                time_Accelerometer_Un + "," +
                                info[3] + ","
                                        + info[4]+ ","
                                        + info[5]+","
                                        + info[6]+","
                                        + info[7]+","
                                        + info[8]+
                                        "\r\n");
                        //*********** added ****************

                        break;
                    case Sensor.TYPE_GRAVITY:
                        /* m/s^2 */
                        s = s + " X[" + info[3] + "]";
                        s = s + " Y[" + info[4] + "]";
                        s = s + " Z[" + info[5] + "]";
                        //*********** added ****************
                        String time_Gravity= String.valueOf(System.currentTimeMillis());

                        MainActivity.writer_Gravity.print(
                                time_Gravity + "," +
                                info[3] + ","
                                        + info[4]+ ","
                                        + info[5]+
                                        "\r\n");
                        //*********** added ****************

                        break;
                    case Sensor.TYPE_GYROSCOPE:
                        /* rad/s */
                        s = s + " X[" + info[3] + "]";
                        s = s + " Y[" + info[4] + "]";
                        s = s + " Z[" + info[5] + "]";
                        //*********** added ****************
                        String time_Gyro = String.valueOf(System.currentTimeMillis());
                        MainActivity.writer_Gyro.print(
                                time_Gyro + ","
                                + info[3] + ","
                                        + info[4]+ ","
                                        + info[5]+
                                        "\r\n");
                        //*********** added ****************

                        break;
                    case Sensor.TYPE_GYROSCOPE_UNCALIBRATED:
                        /* rad/s */
                        s = s + " X[" + info[3] + "]";
                        s = s + " Y[" + info[4] + "]";
                        s = s + " Z[" + info[5] + "]";

                        s = s + " Xdrift[" + info[6] + "]";
                        s = s + " Ydrift[" + info[7] + "]";
                        s = s + " Zdrift[" + info[8] + "]";
                        //*********** added ****************
                        String time_Gyro_Un = String.valueOf(System.currentTimeMillis());

                        MainActivity.writer_Gyro_Un.print(
                                time_Gyro_Un + ","
                                +
                                info[3] + ","
                                        + info[4]+ ","
                                        + info[5]+","
                                        + info[6]+","
                                        + info[7]+","
                                        + info[8]+
                                        "\r\n");
                        //*********** added ****************
                        break;
                    case Sensor.TYPE_LINEAR_ACCELERATION:
                        /* m/s^2 */
                        s = s + " X[" + info[3] + "]";
                        s = s + " Y[" + info[4] + "]";
                        s = s + " Z[" + info[5] + "]";
                        //*********** added ****************
                        String time_Acc_Lin = String.valueOf(System.currentTimeMillis());

                        MainActivity.writer_Acc_Lin.print(
                                time_Acc_Lin + "," +
                                info[3] + ","
                                        + info[4]+ ","
                                        + info[5]+
                                        "\r\n");
                        //*********** added ****************

                        break;
                    case Sensor.TYPE_MAGNETIC_FIELD:
                        /* T */
                        s = s + " X[" + info[3] + "]";
                        s = s + " Y[" + info[4] + "]";
                        s = s + " Z[" + info[5] + "]";
                        //*********** added ****************
                        String time_Mag = String.valueOf(System.currentTimeMillis());

                        MainActivity.writer_Magnetometer.print(
                                time_Mag + "," +
                                        info[3] + ","
                                        + info[4]+ ","
                                        + info[5]+
                                        "\r\n");
                        //*********** added ****************

                        break;
                    case Sensor.TYPE_ROTATION_VECTOR:
                        /* m * sin(theta/2) */
                        s = s + "  X[" + info[3] + "]";
                        s = s + "  Y[" + info[4] + "]";
                        s = s + "  Z[" + info[5] + "]";
                        /* cos(theta/2) */
                        s = s + " Scalar[" + info[6] + "]";
                        String time_Rotation = String.valueOf(System.currentTimeMillis());
                        MainActivity.writer_Rotation.print(
                                time_Rotation + "," +
                                info[3] + ","
                                        + info[4] + ","
                                        + info[5] + ","
                                        + info [6] +
                                        "\r\n");


                        String qx = info[3].toString();
                        String qy = info[4].toString();
                        String qz = info[5].toString();
                        String qw = info[6].toString();
                        float[] rotation_vector =  new float [4];
                        rotation_vector[0]= parseFloat(qw);
                        rotation_vector[1]= parseFloat(qx);
                        rotation_vector[2]= parseFloat(qy);
                        rotation_vector[3]= parseFloat(qz);
                        //float[] deg =  new float [3];
                        //deg = quat2eulerxyz(rotation_vector);
                        //from documentation setting the proper order
/*                        String rotZ = String.valueOf(deg[0]);
                        String rotX = String.valueOf(deg[1]);
                        String rotY = String.valueOf(deg[2]);
                        MainActivityFragment.rotations = setRotationValue (rotX, rotY, rotZ);*/
                        // calculating real quaternions
                        float[] real_Q =  new float [4];
                        SensorManager.getQuaternionFromVector(real_Q, rotation_vector);
                        // handle angles as needed
                        break;
                    case Sensor.TYPE_ORIENTATION:
                        /* Not Available */
                        break;
                    case Sensor.TYPE_STEP_COUNTER:
                        /* Number of steps */
                        s = s + " Steps[" + info[6] + "]";
                        break;
                    case Sensor.TYPE_STEP_DETECTOR:
                        /* Not Available */
                        break;

                    default:
                        break;
                }
            }
            catch (NumberFormatException nfe) {}
            catch (IndexOutOfBoundsException ioobe) {}
        }

        return s;
    }

    // Method for converting the strings of float to strings of integer
    private String floatStrToIntStr(String floatStr) {
        String intStr = null;
        float floatNum = parseFloat(floatStr);
        int intNum = (int) floatNum;
        intStr = String.valueOf(intNum);
        return intStr;
    }
    //************WIP**********************
    public String[] setRotationValue(String rotXStr,String rotYStr, String rotZStr){
        String[] rotStr = new String [3];
        rotStr[0] = floatStrToIntStr(rotXStr);
        rotStr[1] = floatStrToIntStr(rotYStr);
        rotStr[2] = floatStrToIntStr(rotZStr);
        return rotStr;
    }
    /*quaternation to euler in XYZ (seq:123)*/
    float[] quat2eulerxyz(float[] q){
        /*euler-angles*/
//        float psi = (float) atan2( -2.*(q[2]*q[3] - q[0]*q[1]) , q[0]*q[0] - q[1]*q[1]- q[2]*q[2] + q[3]*q[3]);
//        float theta = (float) asin( 2.*(q[1]*q[3] + q[0]*q[2]));
//        float phi =  (float) atan2( 2.*(-q[1]*q[2] + q[0]*q[3]) , q[0]*q[0] + q[1]*q[1] - q[2]*q[2] - q[3]*q[3]);
//        /*save var. by simply pushing them back into the array and return*/
//        q[1] = psi;
//        q[2] = theta;
//        Log.w("WIP", "psi: " + psi + ",theta: " + theta);
//        q[3] = phi;
        float[] angles = new float[3];
        double sqw = q[0]*q[0];
        double sqx = q[1]*q[1];
        double sqy = q[2]*q[2];
        double sqz = q[3]*q[3];
        float conversion = (float) (1/3.14 * 180);
        angles[0] = (float) Math.atan2(2.0 * (q[1]*q[2] + q[3]*q[0]),(sqx - sqy - sqz + sqw));
        angles [0] = angles[0] * conversion;
        angles[1] = (float) Math.atan2(2.0 * (q[2]*q[3] + q[1]*q[0]),(-sqx - sqy + sqz + sqw));
        angles [1] = angles[1] * conversion;
        angles[2] = (float) Math.asin(-2.0 * (q[1]*q[3] - q[2]*q[0])/(sqx + sqy + sqz + sqw));
        angles [2] = angles[2] * conversion;
        Log.w ("WIP", "angles[0]: " + Math.atan2(2.0 * (q[1]*q[2] + q[3]*q[0]),(sqx - sqy - sqz + sqw)) + "angles[1]: " + angles[1]);
        return angles;
    }
    //************WIP**********************

}
