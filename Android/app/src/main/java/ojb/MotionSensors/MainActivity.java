package ojb.MotionSensors;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ojb.MotionSensors.MyHelper.MyGlobals;

/**
 * Created by Administrator on 15/06/2018.
 */

public class MainActivity extends AppCompatActivity {


    //------
    //-----declaring useful variables-----
    public static PrintWriter writer_SENSORS;
    public static PrintWriter writer_Accelerometer;
    public static PrintWriter writer_Rotation;
    public static PrintWriter writer_Gyro;
    public static PrintWriter writer_Acc_Un;
    public static PrintWriter writer_Acc_Lin;
    public static PrintWriter writer_Gravity;
    public static PrintWriter writer_Gyro_Un;
    public static PrintWriter writer_Euler;
    public static PrintWriter writer_RealQuaternion;
    public static PrintWriter writer_RelativeRotation;
    public static PrintWriter writer_Magnetometer;


    public static boolean relative_rotation_on = false;
    private boolean startStop = false;
    public static File SENSORS_PATH_FORKITKAT = new File(Environment.getExternalStorageDirectory() + "/Documents" + File.separator + "mSensors");
    static File path = SENSORS_PATH_FORKITKAT;
    public static final String TAG = "MainActivity";
    //--------- end of declarations-----

    FloatingActionButton btnStartStop;
    FloatingActionButton btnBluetooth;
    Button btn_relative_rotation;
    Context ctx;
    ImageView vg_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //----------------permissions------------------------
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                //do nothing
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
        } else {
            // Permission has already been granted
        }
        //-----------------end new block------------------------
        //-------- file part--------------
        GregorianCalendar datetime = new GregorianCalendar();
        File file = new File(path, "FILE_SENSORS" +
                datetime.get(Calendar.YEAR) + "-" +
                (datetime.get(Calendar.MONTH) + 1) + "-" +
                datetime.get(Calendar.DAY_OF_MONTH) + "-" +
                datetime.get(Calendar.HOUR_OF_DAY) + "-" +
                datetime.get(Calendar.MINUTE) + "-" +
                datetime.get(Calendar.SECOND) + ".txt");

        // file accelerometer
        File file_acc = new File(path, "FILE_SENSOR_ACC" +
                datetime.get(Calendar.YEAR) + "-" +
                (datetime.get(Calendar.MONTH) + 1) + "-" +
                datetime.get(Calendar.DAY_OF_MONTH) + "-" +
                datetime.get(Calendar.HOUR_OF_DAY) + "-" +
                datetime.get(Calendar.MINUTE) + "-" +
                datetime.get(Calendar.SECOND) + ".txt");
        // file rotation
        File file_rotation = new File(path, "FILE_SENSOR_ROTATION" +
                datetime.get(Calendar.YEAR) + "-" +
                (datetime.get(Calendar.MONTH) + 1) + "-" +
                datetime.get(Calendar.DAY_OF_MONTH) + "-" +
                datetime.get(Calendar.HOUR_OF_DAY) + "-" +
                datetime.get(Calendar.MINUTE) + "-" +
                datetime.get(Calendar.SECOND) + ".txt");
        // file gyro
        File file_gyro = new File(path, "FILE_SENSOR_GYRO" +
                datetime.get(Calendar.YEAR) + "-" +
                (datetime.get(Calendar.MONTH) + 1) + "-" +
                datetime.get(Calendar.DAY_OF_MONTH) + "-" +
                datetime.get(Calendar.HOUR_OF_DAY) + "-" +
                datetime.get(Calendar.MINUTE) + "-" +
                datetime.get(Calendar.SECOND) + ".txt");
        // file acc un
        File file_acc_un = new File(path, "FILE_SENSOR_ACCELERATION_UNCALIBRATED" +
                datetime.get(Calendar.YEAR) + "-" +
                (datetime.get(Calendar.MONTH) + 1) + "-" +
                datetime.get(Calendar.DAY_OF_MONTH) + "-" +
                datetime.get(Calendar.HOUR_OF_DAY) + "-" +
                datetime.get(Calendar.MINUTE) + "-" +
                datetime.get(Calendar.SECOND) + ".txt");
        // file acc lin
        File file_acc_lin = new File(path, "FILE_SENSOR_ACCELERATION_LINEAR" +
                datetime.get(Calendar.YEAR) + "-" +
                (datetime.get(Calendar.MONTH) + 1) + "-" +
                datetime.get(Calendar.DAY_OF_MONTH) + "-" +
                datetime.get(Calendar.HOUR_OF_DAY) + "-" +
                datetime.get(Calendar.MINUTE) + "-" +
                datetime.get(Calendar.SECOND) + ".txt");
        // file gravity
        File file_gravity = new File(path, "FILE_SENSOR_GRAVITY" +
                datetime.get(Calendar.YEAR) + "-" +
                (datetime.get(Calendar.MONTH) + 1) + "-" +
                datetime.get(Calendar.DAY_OF_MONTH) + "-" +
                datetime.get(Calendar.HOUR_OF_DAY) + "-" +
                datetime.get(Calendar.MINUTE) + "-" +
                datetime.get(Calendar.SECOND) + ".txt");
        // file gyro un
        File file_gyro_un = new File(path, "FILE_GYRO_UNCALIBRATED" +
                datetime.get(Calendar.YEAR) + "-" +
                (datetime.get(Calendar.MONTH) + 1) + "-" +
                datetime.get(Calendar.DAY_OF_MONTH) + "-" +
                datetime.get(Calendar.HOUR_OF_DAY) + "-" +
                datetime.get(Calendar.MINUTE) + "-" +
                datetime.get(Calendar.SECOND) + ".txt");
        // file gyro un
        File file_euler = new File(path, "FILE_EULER" +
                datetime.get(Calendar.YEAR) + "-" +
                (datetime.get(Calendar.MONTH) + 1) + "-" +
                datetime.get(Calendar.DAY_OF_MONTH) + "-" +
                datetime.get(Calendar.HOUR_OF_DAY) + "-" +
                datetime.get(Calendar.MINUTE) + "-" +
                datetime.get(Calendar.SECOND) + ".txt");
        File file_relative_rotation = new File(path, "FILE_REL_ROT" +
                datetime.get(Calendar.YEAR) + "-" +
                (datetime.get(Calendar.MONTH) + 1) + "-" +
                datetime.get(Calendar.DAY_OF_MONTH) + "-" +
                datetime.get(Calendar.HOUR_OF_DAY) + "-" +
                datetime.get(Calendar.MINUTE) + "-" +
                datetime.get(Calendar.SECOND) + ".txt");
        File file_magnetometer = new File(path, "FILE_MAGN" +
                datetime.get(Calendar.YEAR) + "-" +
                (datetime.get(Calendar.MONTH) + 1) + "-" +
                datetime.get(Calendar.DAY_OF_MONTH) + "-" +
                datetime.get(Calendar.HOUR_OF_DAY) + "-" +
                datetime.get(Calendar.MINUTE) + "-" +
                datetime.get(Calendar.SECOND) + ".txt");
        try {
            writer_SENSORS = new PrintWriter(file, "UTF-8");
            writer_Accelerometer = new PrintWriter(file_acc, "UTF-8");
            writer_Rotation = new PrintWriter(file_rotation, "UTF-8");
            writer_Gyro = new PrintWriter(file_gyro, "UTF-8");
            writer_Acc_Un = new PrintWriter(file_acc_un, "UTF-8");
            writer_Acc_Lin = new PrintWriter(file_acc_lin, "UTF-8");
            writer_Gravity = new PrintWriter(file_gravity, "UTF-8");
            writer_Gyro_Un = new PrintWriter(file_gyro_un, "UTF-8");
            writer_Euler = new PrintWriter(file_euler, "UTF-8");
            writer_RelativeRotation = new PrintWriter(file_relative_rotation, "UTF-8");
            writer_Magnetometer = new PrintWriter(file_magnetometer, "UTF-8");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //-------- end file declaration-------
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
            } else {
                Log.w(TAG, "Permission NOT granted");
            }
        }
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        boolean pathCreated = false;
        if (!MainActivity.path.exists()) {
            pathCreated = MainActivity.path.mkdirs();
        } else Log.d("path", "IT EXISTS");
        Log.d("Ecco il path", MainActivity.path.getAbsolutePath());
        Log.d("PathCreated: ", String.valueOf(pathCreated));
        File file_SENSORSACC = new File(MainActivity.path, "FILE_SENSORACC" + String.valueOf(System.currentTimeMillis()) + ".txt");
        File file_SENSORSGYRO = new File(MainActivity.path, "FILE_SENSORGYRO" + String.valueOf(System.currentTimeMillis()) + ".txt");
        Log.i(TAG, "Ready");
        super.onCreate(savedInstanceState);
        MyGlobals.activity = this;
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ctx = getApplicationContext();


        btnStartStop = (FloatingActionButton) findViewById(R.id.btnStartStop);
        btnStartStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startStop = true;
                Log.d(TAG, "Start button pressed");
                MyGlobals.toggleRunning();
                btnStartStop.setImageResource(MyGlobals.isRunning() ? android.R.drawable.ic_media_pause : android.R.drawable.ic_media_play);
            }
        });

        btnBluetooth = (FloatingActionButton) findViewById(R.id.btnBluetooth);
        btnBluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyGlobals.makeToast(R.string.not_implemented_bluetooth);
            }
        });
        btn_relative_rotation = findViewById(R.id.button_relative_rotation);
        btn_relative_rotation.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 MyGlobals.makeToast(R.string.relative_rotation_is_active);
                 relative_rotation_on = true;
             }
         });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                MyGlobals.makeToast(R.string.not_implemented_settings);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void makeToast(int msg) {
        makeToastRaw(ctx.getResources().getString(msg));
    }

    public void makeToastRaw(String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }

    private void startTracking() {
        startStop = true;
        GregorianCalendar datetime = new GregorianCalendar();
        File file = new File(MainActivity.path, "FILE_SENSORS" +
                datetime.get(Calendar.YEAR) + "-" +
                (datetime.get(Calendar.MONTH) + 1) + "-" +
                datetime.get(Calendar.DAY_OF_MONTH) + "-" +
                datetime.get(Calendar.HOUR_OF_DAY) + "-" +
                datetime.get(Calendar.MINUTE) + "-" +
                datetime.get(Calendar.SECOND) + ".txt");
        try {
            writer_SENSORS = new PrintWriter(file, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    private void stopTraking() {
        startStop = false;
        if (writer_SENSORS != null) writer_SENSORS.close();
    }


}
