package ojb.MotionSensors;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import ojb.MotionSensors.MyHelper.MyMgrSensors;
import ojb.MotionSensors.MyHelper.MyMgrBluetooth;
import ojb.MotionSensors.MyHelper.MyGlobals;

/**
 * Created by Administrator on 15/06/2018.
 */

public class MainActivityFragment extends Fragment {
    //---- added variables
    public TextView rotX;
    public TextView rotY;
    public TextView rotZ;
    public static String[] rotations = new String[3];
    public static String[] relative_rotations = new String[3];

    // text views to display relative rotations
    public TextView rel_rotX;
    public TextView rel_rotY;
    public TextView rel_rotZ;

    private MyMgrSensors mgrSensors = null;
    private MyMgrBluetooth mgrBluetooth = null;

    private boolean doLogSensor = true;

    View thisView = null;
    TextView tvSensorsCount = null;
    LinearLayout layoutSensors = null;
    LinearLayout layoutMainFragment = null;
    LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        thisView = inflater.inflate(R.layout.fragment_main, container, false);

        layoutMainFragment = thisView.findViewById(R.id.layoutMainFragment);
        //indexing rotations in view
        rotX = thisView.findViewById(R.id.rot_x);
        rotY = thisView.findViewById(R.id.rot_y);
        rotZ = thisView.findViewById(R.id.rot_z);
        //indexing rotations in view
        rel_rotX = thisView.findViewById(R.id.rel_rot_x);
        rel_rotY = thisView.findViewById(R.id.rel_rot_y);
        rel_rotZ = thisView.findViewById(R.id.rel_rot_z);

        MyGlobals.fragment = this;
        if (mgrSensors == null)
        {
            mgrSensors = new MyMgrSensors();
        }
        if (mgrBluetooth == null)
        {
            mgrBluetooth = new MyMgrBluetooth();
        }

        return thisView;
    }

    public boolean MyIsRunning()
    {
        return mgrSensors.IsRunning();
    }

    public void MyToggleRunning()
    {
        layoutMainFragment.setVisibility((MyIsRunning() == true) ? View.INVISIBLE : View.VISIBLE);
        mgrSensors.ToggleRunning();
    }

    public void MyProcessSensorEvent(int sensorID, Object [] info)
    {
        String tvSensorTag = "Sensor_" + sensorID;
        String se = mgrSensors.ToString(info);


        if (doLogSensor == true)
        {
            Log.d("Motion Sensors Event", se);
        }
        //Log.w("WIP", "x: " +rotations[0] + ",y: " +rotations[1]);
        rotX.setText(rotations[0]);
        rotY.setText(rotations[1]);
        rotZ.setText(rotations[2]);
        rel_rotX.setText(relative_rotations[0]);
        rel_rotY.setText(relative_rotations[1]);
        rel_rotZ.setText(relative_rotations[2]);

        mgrBluetooth.Send(mgrSensors.ToRawString(info));
    }

    public void MyDisplaySensorsInfo(int SensorsCount)
    {
      //  tvSensorsCount.setText("" + SensorsCount);
    }

    public void setTextInRotx(int angle){

        rotX.setText(String.valueOf(angle));

    }
    public void setTextInRoty(int angle){

        rotY.setText(String.valueOf(angle));

    }
    public void setTextInRotz(int angle){

        rotZ.setText(String.valueOf(angle));

    }
}
