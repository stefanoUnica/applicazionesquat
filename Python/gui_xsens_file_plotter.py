# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'xsens_file_plotter.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from pyqtgraph import PlotWidget


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(544, 280)
        font = QtGui.QFont()
        font.setPointSize(14)
        MainWindow.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../../../../Downloads/Telegram Desktop/logo senza sfondo .png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.checkBox_QuatW = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox_QuatW.setEnabled(True)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.checkBox_QuatW.setFont(font)
        self.checkBox_QuatW.setChecked(True)
        self.checkBox_QuatW.setObjectName("checkBox_QuatW")
        self.gridLayout_2.addWidget(self.checkBox_QuatW, 0, 0, 1, 1)
        self.checkBox_QuatY = QtWidgets.QCheckBox(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.checkBox_QuatY.setFont(font)
        self.checkBox_QuatY.setChecked(True)
        self.checkBox_QuatY.setObjectName("checkBox_QuatY")
        self.gridLayout_2.addWidget(self.checkBox_QuatY, 2, 0, 1, 1)
        self.checkBox_QuatZ = QtWidgets.QCheckBox(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.checkBox_QuatZ.setFont(font)
        self.checkBox_QuatZ.setChecked(True)
        self.checkBox_QuatZ.setObjectName("checkBox_QuatZ")
        self.gridLayout_2.addWidget(self.checkBox_QuatZ, 3, 0, 1, 1)
        self.checkBox_QuatX = QtWidgets.QCheckBox(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.checkBox_QuatX.setFont(font)
        self.checkBox_QuatX.setChecked(True)
        self.checkBox_QuatX.setObjectName("checkBox_QuatX")
        self.gridLayout_2.addWidget(self.checkBox_QuatX, 1, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 4, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 0, 0, 1, 1)
        self.widget = PlotWidget(self.centralwidget)
        self.widget.setObjectName("widget")
        self.gridLayout.addWidget(self.widget, 0, 1, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 544, 18))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionOpen = QtWidgets.QAction(MainWindow)
        self.actionOpen.setObjectName("actionOpen")
        self.menuFile.addAction(self.actionOpen)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Xsens DOT plotter"))
        self.checkBox_QuatW.setText(_translate("MainWindow", "Quat_W"))
        self.checkBox_QuatY.setText(_translate("MainWindow", "Quat_Y"))
        self.checkBox_QuatZ.setText(_translate("MainWindow", "Quat_Z"))
        self.checkBox_QuatX.setText(_translate("MainWindow", "Quat_X"))
        self.label.setText(_translate("MainWindow", "Status:"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.actionOpen.setText(_translate("MainWindow", "Open"))
