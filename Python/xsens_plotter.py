import sys
from PyQt5 import QtCore, QtGui, QtWidgets
import pandas as pd
import pyqtgraph as pg
from gui_xsens_file_plotter import Ui_MainWindow
from tkinter import Tk
from tkinter.filedialog import askopenfilename

global qX_plot
class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, *args, obj=None, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setupUi(self)
        self.widget.setBackground('w')
        self.checkBox_QuatW.stateChanged.connect(self.Check_Answer_Y)
        self.checkBox_QuatX.stateChanged.connect(self.Check_Answer_X)
        self.checkBox_QuatY.stateChanged.connect(self.Check_Answer_Y)
        self.checkBox_QuatZ.stateChanged.connect(self.Check_Answer_Y)

    def Check_Answer_X(self, state):
        if state == QtCore.Qt.Checked:
            print("Wow! You like programming.")
        else:
            global qX_plot
            qX_plot.clear()

    def Check_Answer_Y(self, state):
        if state == QtCore.Qt.Checked:
            print("Wow! You like programming.")
        else:
            global qY_plot
            qY_plot.clear()
def main():

    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    df = pd.read_csv('xsens_data.csv')
    global t
    t = (df['SampleTimeFine'] - df['SampleTimeFine'][0])*10**(-6)
    qW = df['dq_W']
    global qX
    qX = df['dq_X']
    global qY
    qY = df['dq_Y']
    qZ = df['dq_Z']

    pen_green = pg.mkPen(color=(0, 255, 0), width=2)
    qW_plot = window.widget.plot(t, qW, pen = pen_green)
    global pen_red
    pen_red = pg.mkPen(color = (255, 0, 0), width=2)
    global qX_plot
    qX_plot = window.widget.plot(t, qX, pen=pen_red)
    pen_blue = pg.mkPen(color = (0, 0, 255), width=2)
    global qY_plot
    qY_plot = window.widget.plot(t, qY, pen = pen_blue)
    pen_yellow= pg.mkPen(color=(255, 255, 0), width=2)
    window.widget.plot(t, qZ, pen=pen_yellow)
    app.exec()




if __name__ == '__main__':
    main()